﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    public enum Operacija { Kraj = 1,
        UlogujSe = 2,
        VratiKnjige = 3,
        VratiAutore = 4,
        VratiZanrove = 5,
        SacuvajKnjigu = 6,
        PretraziKnjige = 7,
        DeaktivirajKnjigu = 8,
        VratiKorisnike = 9,
        SacuvajKorisnika = 10,
        IzmeniKorisnika = 11,
        PretraziKorisnike = 12,
        VratiPrimerke = 13,
        SacuvajPozajmicu = 14,
        VratiPozajmice = 15,
        DeaktivirajPozajmicu = 16
    }
    [Serializable]
    public class TransfernaKlasa
    {
        public Operacija Operacija { get; set; }
        public Object TransferniObjekat { get; set; }
        public Object Result { get; set; }
    }
}
