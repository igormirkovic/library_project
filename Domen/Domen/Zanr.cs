﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class Zanr: IDomenskiObjekat
    {
        public int ZanrID { get; set; }
        public String Naziv { get; set; }

        public override string ToString()
        {
            return Naziv;
        }
        [Browsable(false)]
        public string Table => "Zanr";
        [Browsable(false)]
        public string FullTable => "Zanr z";
        [Browsable(false)]
        public object Get => "SELECT * FROM";
        [Browsable(false)]
        public string InsertValues => $"'{Naziv}'";
        [Browsable(false)]
        public string UpdateValues => $"Naziv = '{Naziv}'";
        [Browsable(false)]
        public string Join => $"";
        [Browsable(false)]
        public string SearchWhere(string criteria, string criteria1)
        {
            return $"WHERE z.Naziv like '%{criteria}%'";
        }
        [Browsable(false)]
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {
                lista.Add(new Zanr()
                {
                    ZanrID = (int)reader["ZanrID"],
                    Naziv = (string)reader["Naziv"]
                });
            }
            return lista;
        }
        [Browsable(false)]
        public string SearchId => $"where ZanrID = {ZanrID}";
        [Browsable(false)]
        public object ColumnId => "ZanrID";

        [Browsable(false)]
        public string GroupBy => "";
        [Browsable(false)]

        public string Kriterijum { get; set; }

    }
}
