﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class Pozajmica:IDomenskiObjekat
    {
        public int PozajmicaID { get; set; }

        public DateTime DatumKreiranjaPozajmice { get; set; }

        public Korisnik Korisnik { get; set; }
        [Browsable(false)]

        public PrimerakKnjige PrimerakKnjige { get; set; }
        public bool Aktivna { get; set; }

        [Browsable(false)]
        public string Table => "Pozajmica";
        [Browsable(false)]
        public string FullTable => "Pozajmica p";
        [Browsable(false)]
        public object Get => "SELECT p.PozajmicaID as PozajmicaID, k.Naziv as NazivKnjige, k.KnjigaID as KnjigaID, pk.PrimerakKnjigeID as PrimerakKnjigeID, p.DatumKreiranjaPozajmice as DatumKreiranjaPozajmice, ko.KorisnikID as KorisnikID, ko.ImePrezime as ImePrezimeKorisnika, p.Aktivna as AktivnaPozajmica FROM";
        [Browsable(false)]
        public string InsertValues => $"'{DatumKreiranjaPozajmice}', {Korisnik.KorisnikID}, {PrimerakKnjige.PrimerakKnjigeID}, '{Aktivna}'";
        [Browsable(false)]
        public string UpdateValues => $"DatumKreiranjaPozajmice = '{DatumKreiranjaPozajmice}', KorisnikID={Korisnik.KorisnikID}, PrimerakKnjigeID={PrimerakKnjige.PrimerakKnjigeID}, Aktivna='{Aktivna}'";
        [Browsable(false)]
        public string Join => $"LEFT JOIN PrimerakKnjige pk on(p.PrimerakKnjigeID = pk.PrimerakKnjigeID) LEFT JOIN Knjiga k on(k.KnjigaID= pk.KnjigaID) LEFT JOIN Korisnik ko on(ko.KorisnikID = p.KorisnikID)";
        [Browsable(false)]
        public string SearchWhere(string criteria, string criteria1)
        {
            return $"WHERE k.Naziv like '%{criteria}%' or ko.ImePrezime like '%{criteria}%' or p.DatumKreiranjaPozajmice like '%{criteria}%'";
        }
        [Browsable(false)]
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {
                lista.Add(new Pozajmica()
                {
                    PozajmicaID = (int)reader["PozajmicaID"],
                    PrimerakKnjige = new PrimerakKnjige()
                    {
                        PrimerakKnjigeID = (int)reader["PrimerakKnjigeID"],
                        Knjiga = new Knjiga()
                        {
                            KnjigaID = (int)reader["KnjigaID"],
                            Naziv = (string)reader["NazivKnjige"]
                        }
                    },
                    DatumKreiranjaPozajmice = (DateTime)reader["DatumKreiranjaPozajmice"],
                    Aktivna = (bool)reader["AktivnaPozajmica"],
                    Korisnik = new Korisnik()
                    {
                        KorisnikID = (int)reader["KorisnikID"],
                        ImePrezime = (string)reader["ImePrezimeKorisnika"]
                    }
                });
            }
            reader.Close();
            return lista;
        }
        [Browsable(false)]
        public string SearchId => $"where PozajmicaID = {PozajmicaID}";
        [Browsable(false)]
        public object ColumnId => "PozajmicaID";
        [Browsable(false)]
        public string GroupBy => "";

        [Browsable(false)]
        public string Kriterijum { get; set; }


        /*Showing for datagrid props*/

        public int PrimerakKnjigeID 
        {
            get
            {
                return PrimerakKnjige.PrimerakKnjigeID;
            }
        }

        public string NazivKnjige
        {
            get
            {
                return PrimerakKnjige.Knjiga.Naziv;
            }
        }

        

    }
}
