﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class PrimerakKnjige:IDomenskiObjekat
    {
        public int PrimerakKnjigeID { get; set; } 
        public Knjiga Knjiga { get; set; }
        public DateTime DatumIzdavanja { get; set; }
        public bool Zauzet { get; set; }

        [Browsable(false)]
        public string Table => "PrimerakKnjige";
        [Browsable(false)]
        public string FullTable => "PrimerakKnjige pk";
        [Browsable(false)]
        public object Get => "SELECT * FROM";
        [Browsable(false)]
        public string InsertValues => $"{Knjiga.KnjigaID}, {DatumIzdavanja}, '{Zauzet}'";
        [Browsable(false)]
        public string UpdateValues => $"KnjigaID = {Knjiga.KnjigaID}, DatumIzdavanja='{DatumIzdavanja}', Zauzet='{Zauzet}'";
        [Browsable(false)]
        public string Join => $"";
        [Browsable(false)]
        public string SearchWhere(string criteria, string criteria1)
        {
            return $"WHERE KnjigaID={criteria} AND Zauzet='False'";
        }
        [Browsable(false)]
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {
                lista.Add(new PrimerakKnjige()
                {
                    PrimerakKnjigeID = (int)reader["PrimerakKnjigeID"],
                    Knjiga = new Knjiga()
                    {
                        KnjigaID = (int)reader["KnjigaID"]
                    },
                    DatumIzdavanja = (DateTime)reader["DatumIzdavanja"],
                    Zauzet = (bool)reader["Zauzet"]
                });
            }
            reader.Close();
            return lista;
        }
        [Browsable(false)]
        public string SearchId => $"WHERE PrimerakKnjigeID = {PrimerakKnjigeID}";
        [Browsable(false)]
        public object ColumnId => "PrimerakKnjigeID";
        [Browsable(false)]
        public string GroupBy => "";

        [Browsable(false)]
        public string Kriterijum { get; set; }

        public override string ToString()
        {
            return $"{DatumIzdavanja.ToString("dd.MM.yyyy.")}";
        }

    }
}
