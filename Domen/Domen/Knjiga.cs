﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class Knjiga : IDomenskiObjekat
    {
        [Browsable(false)]
        public int KnjigaID { get; set; }
        public String Naziv { get; set; }
        public Autor Autor { get; set; }
        public Zanr Zanr { get; set; }
        public bool Aktivna { get; set; }
        public int BrojPrimeraka { get; set; }
        [Browsable(false)]
        public string Table => "Knjiga";
        [Browsable(false)]
        public string FullTable => "Knjiga k";
        [Browsable(false)]
        public object Get => "SELECT k.KnjigaID as KnjigaID, k.Naziv as Naziv, k.Aktivna as Aktivna, z.Naziv as NazivZanra, z.ZanrID as ZanrID, a.AutorID as AutorID, a.ImePrezime as ImePrezime, count(pk.KnjigaID) as BrojPrimeraka FROM";
        [Browsable(false)]
        public string InsertValues => $"'{Naziv}', {Autor.AutorID}, {Zanr.ZanrID}, '{Aktivna}'";
        [Browsable(false)]
        public string UpdateValues => $"Naziv = '{Naziv}', AutorID={Autor.AutorID}, ZanrID={Zanr.ZanrID}, Aktivna='{Aktivna}'";
        [Browsable(false)]
        public string Join => $"JOIN Autor a on(a.AutorID = k.AutorID) JOIN Zanr z on(z.ZanrID = k.ZanrID) LEFT JOIN PrimerakKnjige pk on(pk.KnjigaID = k.KnjigaID)";
        [Browsable(false)]
        public string SearchWhere(string criteria, string criteria1)
        {
            return $"WHERE k.Naziv like '%{criteria}%' or a.ImePrezime like '%{criteria}%' or z.Naziv like '%{criteria}%'";
        }
        [Browsable(false)]
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {
                lista.Add(new Knjiga()
                {
                    KnjigaID = (int)reader["KnjigaID"],
                    Naziv = (string)reader["Naziv"],
                    Autor = new Autor()
                    {
                        AutorID = (int)reader["AutorID"],
                        ImePrezime = (string)reader["ImePrezime"],

                    },
                    Zanr = new Zanr()
                    {
                        ZanrID = (int)reader["ZanrID"],
                        Naziv = (string)reader["NazivZanra"]
                    },
                    Aktivna = (bool)reader["Aktivna"],
                    BrojPrimeraka = (int)reader["BrojPrimeraka"]
                });
            }
            reader.Close();
            return lista;
        }
        [Browsable(false)]
        public string SearchId => $"where KnjigaID = {KnjigaID}";
        [Browsable(false)]
        public object ColumnId => "KnjigaID";
        [Browsable(false)]
        public string GroupBy => "GROUP BY k.KnjigaID, k.Naziv, k.Aktivna, z.Naziv, a.ImePrezime, z.ZanrID, a.AutorID";
       
        [Browsable(false)]
        public string Kriterijum { get; set; }

        public override string ToString()
        {
            return $"{Naziv}";
        }

    }
}
