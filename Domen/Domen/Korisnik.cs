﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class Korisnik:IDomenskiObjekat
    {
        public int KorisnikID { get; set; }
        public String ImePrezime { get; set; }
        public DateTime DatumRodjenja{ get; set; }
        public String Pol { get; set; }
        public String StrucnaSprema { get; set; }
        public bool Aktivan { get; set; }

        [Browsable(false)]
        public string Table => "Korisnik";
        [Browsable(false)]
        public string FullTable => "Korisnik k";
        [Browsable(false)]
        public object Get => "SELECT * FROM";
        [Browsable(false)]
        public string InsertValues => $"'{ImePrezime}','{DatumRodjenja}','{Pol}','{StrucnaSprema}','{Aktivan}'";
        [Browsable(false)]
        public string UpdateValues => $"ImePrezime= '{ImePrezime}', DatumRodjenja='{DatumRodjenja}', Pol='{Pol}', StrucnaSprema='{StrucnaSprema}', Aktivan='{Aktivan}'";
        [Browsable(false)]
        public string Join => $"";
        [Browsable(false)]
        public string SearchWhere(string kriterijum, string kriterijum1)
        {
            return $"WHERE ImePrezime like '%{kriterijum}%' or StrucnaSprema like '%{kriterijum}%' or Pol like '%{kriterijum}%'";
        }
        [Browsable(false)]
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {

                lista.Add(new Korisnik()
                {
                    KorisnikID = (int)reader["KorisnikID"],
                    ImePrezime = (string)reader["ImePrezime"],
                    DatumRodjenja = (DateTime)reader["DatumRodjenja"],
                    Pol = (string)reader["Pol"],
                    StrucnaSprema = (string)reader["StrucnaSprema"],
                    Aktivan = (bool)reader["Aktivan"]
                });
            }
            reader.Close();
            return lista;
        }

        [Browsable(false)]
        public string SearchId => $"WHERE KorisnikID = {KorisnikID}";
        [Browsable(false)]
        public object ColumnId => "KorisnikID";
        [Browsable(false)]

        public string Kriterijum { get; set; }
        [Browsable(false)]
        public string GroupBy => "";

        public override string ToString()
        {
            return $"{ImePrezime}";
        }



    }
}
