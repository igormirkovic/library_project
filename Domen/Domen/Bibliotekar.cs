﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class Bibliotekar: IDomenskiObjekat
    {
        public int BibliotekarID { get; set; }
        public String ImePrezime { get; set; }
        public DateTime DatumRodjenja { get; set; }
        public String KorisnickoIme { get; set; }
        public String Lozinka { get; set; }
        [Browsable(false)]
        public string Table => "Bibliotekar ";

        [Browsable(false)]
        public string FullTable => "Bibliotekar b";
        [Browsable(false)]
        public object Get => "SELECT * FROM";
        public string InsertValues => $"'{ImePrezime}','{DatumRodjenja}','{KorisnickoIme}','{Lozinka}'";
        public string UpdateValues => $"ImePrezime={ImePrezime}', DatumRodjenja='{DatumRodjenja}', KorisnickoIme='{KorisnickoIme}', Lozinka='{Lozinka}'";
        public string Join => $"";
        public string SearchWhere(string KorisnickoIme, string Lozinka)
        {
            return $"WHERE KorisnickoIme='{KorisnickoIme}' AND Lozinka='{Lozinka}'";
        }
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {

                lista.Add(new Bibliotekar()
                    {
                        ImePrezime = (string)reader["ImePrezime"],
                        KorisnickoIme = (string)reader["KorisnickoIme"],
                        Lozinka = (string)reader["Lozinka"],
                        BibliotekarID = (int)reader["BibliotekarID"]
                    });
            }
            reader.Close();
            return lista;
        }

        [Browsable(false)]
        public string SearchId => $"where BibliotekarID = {BibliotekarID}";
        [Browsable(false)]
        public object ColumnId => "BibliotekarID";
        [Browsable(false)]

        public string Kriterijum { get; set; }
        [Browsable(false)]
        public string GroupBy => "";


    }
}
