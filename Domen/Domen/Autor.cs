﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domen
{
    [Serializable]
    public class Autor:IDomenskiObjekat
    {
        public int AutorID { get; set; }
        public String ImePrezime { get; set; }

        public override string ToString()
        {
            return $"{ImePrezime}";
        }
        [Browsable(false)]
        public string Table => "Autor";

        [Browsable(false)]
        public string FullTable => "Autor a";
        [Browsable(false)]

        public object Get => "SELECT * FROM";
        [Browsable(false)]
        public string InsertValues => $"'{ImePrezime}'";
        [Browsable(false)]
        public string UpdateValues => $"ImePrezime = '{ImePrezime}'";
        [Browsable(false)]
        public string Join => $"";
        [Browsable(false)]
        public string SearchWhere(string criteria, string criteria1)
        {
            return $"WHERE a.ImePrezime like '%{criteria}%'";
        }
        [Browsable(false)]
        public List<IDomenskiObjekat> GetReaderResult(SqlDataReader reader)
        {
            List<IDomenskiObjekat> lista = new List<IDomenskiObjekat>();
            while (reader.Read())
            {
                lista.Add(new Autor()
                {
                    AutorID = (int)reader["AutorID"],
                    ImePrezime = (string)reader["ImePrezime"]
                });
            }
            return lista;
        }
        [Browsable(false)]
        public string SearchId => $"where AutorID = {AutorID}";
        [Browsable(false)]
        public object ColumnId => "AutorID";
        [Browsable(false)]
        public string GroupBy => "";
        [Browsable(false)]

        public string Kriterijum { get; set; }

    }
}
