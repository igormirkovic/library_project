﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace SistemskeOperacije
{
    public class DeaktivirajKnjiguSO: OpstaSistemskaOperacija
    {
        public Knjiga Knjiga { get; private set; }
        protected override object IzvrsiKonkretnuOperaciju(IDomenskiObjekat objekat)
        {
            Knjiga k = (Knjiga)objekat;
            Knjiga = k;
            return broker.Izmeni(objekat);

        }

        protected override void Validacija(IDomenskiObjekat objekat)
        {
            if (!(objekat is Knjiga))
            {
                throw new ArgumentException();
            }
        }
    }
}
