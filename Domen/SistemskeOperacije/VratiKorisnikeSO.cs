﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace SistemskeOperacije
{
    public class VratiKorisnikeSO: OpstaSistemskaOperacija
    {
        public Korisnik Korisnik { get; private set; }
        protected override object IzvrsiKonkretnuOperaciju(IDomenskiObjekat objekat)
        {
            Korisnik k = (Korisnik)objekat;
            Korisnik = k;
            return broker.VratiSve(objekat).OfType<Korisnik>().ToList();

        }

        protected override void Validacija(IDomenskiObjekat objekat)
        {
            if (!(objekat is Korisnik))
            {
                throw new ArgumentException();
            }
        }
    }
}
