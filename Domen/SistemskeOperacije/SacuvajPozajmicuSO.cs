﻿using Domen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemskeOperacije
{
    public class SacuvajPozajmicuSO:OpstaSistemskaOperacija
    {
        public Pozajmica Pozajmica { get; private set; }
        protected override object IzvrsiKonkretnuOperaciju(IDomenskiObjekat objekat)
        {
            Pozajmica p = (Pozajmica)objekat;
            Pozajmica = p;
            return broker.Sacuvaj(objekat);

        }

        protected override void Validacija(IDomenskiObjekat objekat)
        {
            if (!(objekat is Pozajmica))
            {
                throw new ArgumentException();
            }
        }
    }
}
