﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace SistemskeOperacije
{
    public class DeaktivirajPozajmicuSO: OpstaSistemskaOperacija
    {
        public Pozajmica Pozajmica { get; private set; }
        protected override object IzvrsiKonkretnuOperaciju(IDomenskiObjekat objekat)
        {
            Pozajmica k = (Pozajmica)objekat;
            Pozajmica = k;
            return broker.Izmeni(objekat);

        }

        protected override void Validacija(IDomenskiObjekat objekat)
        {
            if (!(objekat is Pozajmica))
            {
                throw new ArgumentException();
            }
        }
    }
}
