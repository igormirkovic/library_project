﻿using Domen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemskeOperacije
{
    public class SacuvajKnjiguSO: OpstaSistemskaOperacija
    {
        public Knjiga Knjiga { get; private set; }
        protected override object IzvrsiKonkretnuOperaciju(IDomenskiObjekat objekat)
        {
            Knjiga k = (Knjiga)objekat;
            Knjiga = k;
            return broker.Sacuvaj(objekat);
            
        }

        protected override void Validacija(IDomenskiObjekat objekat)
        {
            if (!(objekat is Knjiga))
            {
                throw new ArgumentException();
            }
        }
    }
}
