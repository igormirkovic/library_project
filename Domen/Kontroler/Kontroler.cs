﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;
using BrokerBazePodataka;
using SistemskeOperacije;

namespace Kontroler
{
    public class Kontroler
    {
        private Broker broker = Broker.Instance;
        public static Kontroler _instance;
        public static Kontroler Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Kontroler();
                }
                return _instance;
            }
        }

        public Bibliotekar Prijava(string KorisnickoIme, string Lozinka)
        {
            try
            {
                broker.OtvoriKonekciju();
                Bibliotekar bibliotekar = broker.UlogujSe(new Bibliotekar() {KorisnickoIme = KorisnickoIme, Lozinka = Lozinka }).OfType<Bibliotekar>().ToList().FirstOrDefault();
                if (bibliotekar != null)
                {
                    if (bibliotekar.KorisnickoIme == KorisnickoIme && bibliotekar.Lozinka == Lozinka)
                    {
                        return bibliotekar;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch(Exception)
            {
                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
            
        }

        public List<Knjiga> VratiKnjige()
        {
            VratiKnjigeSO so = new VratiKnjigeSO();
            try
            {
                broker.OtvoriKonekciju();
                return so.Izvrsi(new Knjiga()) as List<Knjiga>;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public List<Zanr> VratiZanrove()
        {
            try
            {
                broker.OtvoriKonekciju();
                return broker.VratiSve(new Zanr()).OfType<Zanr>().ToList();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public List<Autor> VratiAutore()
        {
            try
            {
                broker.OtvoriKonekciju();
                return broker.VratiSve(new Autor()).OfType<Autor>().ToList();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public List<Korisnik> VratiKorisnike()
        {
            VratiKorisnikeSO so = new VratiKorisnikeSO();
            try
            {
                broker.OtvoriKonekciju();
                return so.Izvrsi(new Korisnik()) as List<Korisnik>;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public List<PrimerakKnjige> VratiPrimerke(Knjiga knjiga)
        {
            try
            {
                broker.OtvoriKonekciju();
                return broker.VratiSvePoKriterijumu(new PrimerakKnjige(), knjiga).OfType<PrimerakKnjige>().ToList();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public bool SacuvajKnjigu(IDomenskiObjekat objekat)
        {
            SacuvajKnjiguSO so = new SacuvajKnjiguSO();
            try
            {
                broker.OtvoriKonekciju();
                return (bool)so.Izvrsi(objekat);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public List<Knjiga> PretraziKnjige(IDomenskiObjekat objekat)
        {
            PretraziKnjigeSO so = new PretraziKnjigeSO();
            try
            {
                broker.OtvoriKonekciju();
                return so.Izvrsi(objekat) as List<Knjiga>;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public object SacuvajKorisnika(IDomenskiObjekat objekat)
        {
            SacuvajKorisnikaSO so = new SacuvajKorisnikaSO();
            try
            {
                broker.OtvoriKonekciju();
                return (bool)so.Izvrsi(objekat);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public bool IzmeniKorisnika(IDomenskiObjekat objekat)
        {
            IzmeniKorisnikaSO so = new IzmeniKorisnikaSO();
            try
            {
                broker.OtvoriKonekciju();
                return (bool)so.Izvrsi(objekat);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public bool SacuvajPozajmicu(Pozajmica pozajmica)
        {
            SacuvajPozajmicuSO so = new SacuvajPozajmicuSO();
            try
            {
                broker.OtvoriKonekciju();
                return (bool)so.Izvrsi(pozajmica);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public object VratiPozajmice()
        {
            try
            {
                broker.OtvoriKonekciju();
                return broker.VratiSve(new Pozajmica()).OfType<Pozajmica>().ToList();

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public bool DeaktivirajPozajmicu(IDomenskiObjekat objekat)
        {
            DeaktivirajPozajmicuSO so = new DeaktivirajPozajmicuSO();
            try
            {
                broker.OtvoriKonekciju();
                return (bool)so.Izvrsi(objekat);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public bool IzmeniPrimerakKnjige(IDomenskiObjekat objekat)
        {
            Pozajmica pozajmica = (Pozajmica)objekat;
            PrimerakKnjige primerakKnjige = pozajmica.PrimerakKnjige;
            primerakKnjige.Zauzet = true;
            try
            {
                broker.OtvoriKonekciju();
                return (bool)broker.Izmeni(primerakKnjige);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        public List<Korisnik> PretraziKorisnike(IDomenskiObjekat objekat)
        {
            PretraziKorisnikeSO so = new PretraziKorisnikeSO();
            try
            {
                broker.OtvoriKonekciju();
                return so.Izvrsi(objekat) as List<Korisnik>;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }

        

        public bool DeaktivirajKnjigu(IDomenskiObjekat objekat)
        {
            DeaktivirajKnjiguSO so = new DeaktivirajKnjiguSO();
            try
            {
                broker.OtvoriKonekciju();
                return (bool)so.Izvrsi(objekat);
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                broker.ZatvoriKonekciju();
            }
        }
    }
}
