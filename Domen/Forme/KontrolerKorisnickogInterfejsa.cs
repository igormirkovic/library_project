﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace Forme
{
    public class KontrolerKorisnickogInterfejsa
    {
        Komunikacija k;

        public static KontrolerKorisnickogInterfejsa _instance;
        public static KontrolerKorisnickogInterfejsa Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new KontrolerKorisnickogInterfejsa();
                }
                return _instance;
            }
        }

        

        KontrolerKorisnickogInterfejsa()
        {
            k = new Komunikacija();
        }

        public Bibliotekar UlogujSe(String KorisnickoIme, String Lozinka)
        {
            return k.UlogujSe(KorisnickoIme, Lozinka);
        }

        public bool DeaktivirajPozajmicu(Pozajmica pozajmica)
        {
            pozajmica.Aktivna = false;
            return k.DeaktivirajPozajmicu(pozajmica);
        }

        public List<PrimerakKnjige> VratiPrimerke(Knjiga knjiga)
        {
            return k.VratiPrimerke(knjiga);
        }

        public List<Knjiga> VratiSveKnjige()
        {
            return k.VratiSveKnjige();
        }

        public List<Pozajmica> VratiPozajmice()
        {
            return k.VratiSvePozajmice();
        }

        public List<Knjiga> PretraziKnjigu(string kriterijum)
        {
            return k.PretraziKnjige(kriterijum);
        }

        public bool DeaktivirajKnjigu(Knjiga knjiga)
        {
            knjiga.Aktivna = false;
            return k.DeaktivirajKnjigu(knjiga);
        }

        public List<Autor> VratiSveAutore()
        {
            return k.VratiSveAutore();
        }

        public bool SacuvajPozajmicu(Pozajmica pozajmica)
        {
            return k.SacuvajPozajmicu(pozajmica);
        }

        public List<Zanr> VratiSveZanrove()
        {
            return k.VratiSveZanrove();
        }

        public List<Korisnik> VratiSveKorisnike()
        {
            return k.VratiSveKorisnike();
        }

        public List<Korisnik> PretraziKorisnika(string kriterijum)
        {
            return k.PretraziKorisnike(kriterijum);
        }

        public void Kraj()
        {
            k.Kraj();
        }

        public bool PoveziSeNaServer()
        {
            return k.PoveziSeNaServer();
        }

        public bool SacuvajKnjigu(Knjiga knjiga)
        {
            return k.SacuvajKnjigu(knjiga);
        }

        public bool SacuvajKorisnika(Korisnik korisnik)
        {
            return k.SacuvajKorisnika(korisnik);
        }

        public bool IzmeniKorisnika(Korisnik korisnik)
        {
            return k.IzmeniKorisnika(korisnik);
        }
    }
}
