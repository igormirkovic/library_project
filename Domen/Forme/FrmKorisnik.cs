﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forme
{
    public partial class FrmKorisnik : Form
    {
        bool kliknutoDugmePretrazi = false;
        public FrmKorisnik()
        {
            InitializeComponent();
            dgv_Korisnici.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiSveKorisnike();
            Timer t;
            t = new Timer();
            t.Interval = 5000;
            t.Tick += prikaziKorisnike;
            t.Start();
        }

        private void prikaziKorisnike(object sender, EventArgs e)
        {
            if (!kliknutoDugmePretrazi)
            {
                dgv_Korisnici.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiSveKorisnike();
            }
            else
            {
                dgv_Korisnici.DataSource = KontrolerKorisnickogInterfejsa.Instance.PretraziKorisnika(txt_PretraziKorisnika.Text);
            }
        }

        private void btn_KreirajKorisnika_Click(object sender, EventArgs e)
        {
            new FrmKreirajPromeniKorisnika().ShowDialog();
        }

        private void btn_PromeniKorisnika_Click(object sender, EventArgs e)
        {
            try
            {
                new FrmKreirajPromeniKorisnika((Korisnik)dgv_Korisnici.SelectedRows[0].DataBoundItem).ShowDialog();
            }
            catch (Exception)
            {
                MessageBox.Show("Niste odabrali nijednog korisnika.");
            }
        }

        private void btn_PretraziKorisnika_Click(object sender, EventArgs e)
        {
          kliknutoDugmePretrazi = true;
          dgv_Korisnici.DataSource = KontrolerKorisnickogInterfejsa.Instance.PretraziKorisnika(txt_PretraziKorisnika.Text);
            if(KontrolerKorisnickogInterfejsa.Instance.PretraziKorisnika(txt_PretraziKorisnika.Text).Count == 0)
            {
                MessageBox.Show("Sistem ne moze da nadje korisnika po zadatoj vrednosti.");
            }
            
        }

        private void knjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FrmPozajmice().ShowDialog();
            this.Close();
        }

        private void knjigeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FrmPozajmice().ShowDialog();
            this.Close();
        }

        
    }
}
