﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace Forme
{
    public class Sesija
    {
        public static Sesija _instance;
        public static Sesija Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Sesija();
                }
                return _instance;
            }
        }

        public Bibliotekar Bibliotekar { get; set; }
    }
}
