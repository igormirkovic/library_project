﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using Domen;

namespace Forme
{
    public class Komunikacija
    {
        TcpClient klijent;
        NetworkStream tok;
        BinaryFormatter formatter;
        public bool PoveziSeNaServer()
        {
            try
            {
                klijent = new TcpClient("127.0.0.1", 20000);
                tok = klijent.GetStream();
                formatter = new BinaryFormatter();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        

        private TransfernaKlasa ProcitajOdgovor()
        {
            try
            {
                return (TransfernaKlasa)formatter.Deserialize(tok);
            }
            catch (Exception e)
            {
                Debug.WriteLine(">>>" + e.Message);
                return null;
            }
        }

        public bool DeaktivirajPozajmicu(Pozajmica pozajmica)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.DeaktivirajPozajmicu;
            transfernaKlasa.TransferniObjekat = pozajmica;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (bool)transfernaKlasa.Result;
        }

        public List<PrimerakKnjige> VratiPrimerke(Knjiga knjiga)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.VratiPrimerke;
            transfernaKlasa.TransferniObjekat = knjiga;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<PrimerakKnjige>)transfernaKlasa.Result;
        }

        public List<Pozajmica> VratiSvePozajmice()
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.VratiPozajmice;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Pozajmica>)transfernaKlasa.Result;
        }

        public Bibliotekar UlogujSe(String KorisnickoIme, String Lozinka)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.UlogujSe;
            Bibliotekar bibliotekar = new Bibliotekar()
            {
                KorisnickoIme = KorisnickoIme,
                Lozinka = Lozinka
            };
            transfernaKlasa.TransferniObjekat = bibliotekar;
            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (Bibliotekar)transfernaKlasa.TransferniObjekat;
        }

        public bool SacuvajPozajmicu(Pozajmica pozajmica)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.SacuvajPozajmicu;
            transfernaKlasa.TransferniObjekat = pozajmica;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (bool)transfernaKlasa.Result;
        }

        public List<Korisnik> PretraziKorisnike(string kriterijum)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.PretraziKorisnike;
            transfernaKlasa.TransferniObjekat = kriterijum;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Korisnik>)transfernaKlasa.Result;
        }

        public List<Korisnik> VratiSveKorisnike()
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.VratiKorisnike;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Korisnik>)transfernaKlasa.TransferniObjekat;
        }

        

        public bool SacuvajKorisnika(Korisnik korisnik)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.SacuvajKorisnika;
            transfernaKlasa.TransferniObjekat = korisnik;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (bool)transfernaKlasa.Result;
        }

        public bool IzmeniKorisnika(Korisnik korisnik)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.IzmeniKorisnika;
            transfernaKlasa.TransferniObjekat = korisnik;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (bool)transfernaKlasa.Result;
        }

        public bool SacuvajKnjigu(Knjiga knjiga)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.SacuvajKnjigu;
            transfernaKlasa.TransferniObjekat = knjiga;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (bool)transfernaKlasa.Result;
        }

        public List<Knjiga> PretraziKnjige(string kriterijum)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.PretraziKnjige;
            transfernaKlasa.TransferniObjekat = kriterijum;
            
            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Knjiga>)transfernaKlasa.Result ;
        }

        public List<Knjiga> VratiSveKnjige()
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.VratiKnjige;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Knjiga>)transfernaKlasa.TransferniObjekat;

        }

        public bool DeaktivirajKnjigu(Knjiga knjiga)
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.DeaktivirajKnjigu;
            transfernaKlasa.TransferniObjekat = knjiga;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (bool)transfernaKlasa.Result;
        }

        public List<Autor> VratiSveAutore()
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.VratiAutore;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Autor>)transfernaKlasa.TransferniObjekat;
        }

        public List<Zanr> VratiSveZanrove()
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.VratiZanrove;

            formatter.Serialize(tok, transfernaKlasa);
            transfernaKlasa = ProcitajOdgovor();
            return (List<Zanr>)transfernaKlasa.TransferniObjekat;
        }


        public void Kraj()
        {
            TransfernaKlasa transfernaKlasa = new TransfernaKlasa();
            transfernaKlasa.Operacija = Operacija.Kraj;
            formatter.Serialize(tok,transfernaKlasa);
        }
    }
}
