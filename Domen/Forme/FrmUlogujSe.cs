﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Domen;

namespace Forme
{
    public partial class FrmUlogujSe : Form
    {
        KontrolerKorisnickogInterfejsa kontroler = KontrolerKorisnickogInterfejsa.Instance;
        Sesija sesija = Sesija.Instance;
        public FrmUlogujSe()
        {
            InitializeComponent();
        }

        private void btn_UlogujSe_Click(object sender, EventArgs e)
        {
            if (kontroler.PoveziSeNaServer())
            {
                Bibliotekar bibliotekar = kontroler.UlogujSe(txt_KorisnickoIme.Text, txt_Lozinka.Text);
                if (bibliotekar != null)
                {
                    MessageBox.Show("Uspesno ulogovan !");
                    this.Hide();
                    sesija.Bibliotekar = bibliotekar;
                    new FrmGlavnaForma().ShowDialog();
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Pokusajte ponovo!");
                    txt_KorisnickoIme.Text = "";
                    txt_Lozinka.Text = "";
                    txt_KorisnickoIme.Focus();

                }
            }
        }
    }
}
