﻿namespace Forme
{
    partial class FrmUlogujSe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_UlogujSe = new System.Windows.Forms.Button();
            this.lbl_KorisnickoIme = new System.Windows.Forms.Label();
            this.lbl_Loznika = new System.Windows.Forms.Label();
            this.txt_KorisnickoIme = new System.Windows.Forms.TextBox();
            this.txt_Lozinka = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_UlogujSe
            // 
            this.btn_UlogujSe.Location = new System.Drawing.Point(105, 201);
            this.btn_UlogujSe.Name = "btn_UlogujSe";
            this.btn_UlogujSe.Size = new System.Drawing.Size(146, 90);
            this.btn_UlogujSe.TabIndex = 0;
            this.btn_UlogujSe.Text = "Uloguj se";
            this.btn_UlogujSe.UseVisualStyleBackColor = true;
            this.btn_UlogujSe.Click += new System.EventHandler(this.btn_UlogujSe_Click);
            // 
            // lbl_KorisnickoIme
            // 
            this.lbl_KorisnickoIme.AutoSize = true;
            this.lbl_KorisnickoIme.Location = new System.Drawing.Point(75, 90);
            this.lbl_KorisnickoIme.Name = "lbl_KorisnickoIme";
            this.lbl_KorisnickoIme.Size = new System.Drawing.Size(81, 13);
            this.lbl_KorisnickoIme.TabIndex = 1;
            this.lbl_KorisnickoIme.Text = "Korisničko ime: ";
            // 
            // lbl_Loznika
            // 
            this.lbl_Loznika.AutoSize = true;
            this.lbl_Loznika.Location = new System.Drawing.Point(75, 135);
            this.lbl_Loznika.Name = "lbl_Loznika";
            this.lbl_Loznika.Size = new System.Drawing.Size(47, 13);
            this.lbl_Loznika.TabIndex = 2;
            this.lbl_Loznika.Text = "Lozinka:";
            // 
            // txt_KorisnickoIme
            // 
            this.txt_KorisnickoIme.Location = new System.Drawing.Point(184, 87);
            this.txt_KorisnickoIme.Name = "txt_KorisnickoIme";
            this.txt_KorisnickoIme.Size = new System.Drawing.Size(134, 20);
            this.txt_KorisnickoIme.TabIndex = 3;
            this.txt_KorisnickoIme.Text = "igormirkovicc";
            // 
            // txt_Lozinka
            // 
            this.txt_Lozinka.Location = new System.Drawing.Point(184, 132);
            this.txt_Lozinka.Name = "txt_Lozinka";
            this.txt_Lozinka.PasswordChar = '•';
            this.txt_Lozinka.Size = new System.Drawing.Size(134, 20);
            this.txt_Lozinka.TabIndex = 4;
            this.txt_Lozinka.Text = "igor";
            // 
            // FrmUlogujSe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(373, 365);
            this.Controls.Add(this.txt_Lozinka);
            this.Controls.Add(this.txt_KorisnickoIme);
            this.Controls.Add(this.lbl_Loznika);
            this.Controls.Add(this.lbl_KorisnickoIme);
            this.Controls.Add(this.btn_UlogujSe);
            this.Name = "FrmUlogujSe";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_UlogujSe;
        private System.Windows.Forms.Label lbl_KorisnickoIme;
        private System.Windows.Forms.Label lbl_Loznika;
        private System.Windows.Forms.TextBox txt_KorisnickoIme;
        private System.Windows.Forms.TextBox txt_Lozinka;
    }
}

