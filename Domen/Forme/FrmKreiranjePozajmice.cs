﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forme
{
    public partial class FrmKreiranjePozajmice : Form
    {
        public FrmKreiranjePozajmice()
        {
            InitializeComponent();
            cmb_Korisnik.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiSveKorisnike();
            cmb_Knjiga.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiSveKnjige();
            txt_DatumKreiranjaPozajmice.Text = DateTime.Now.ToString("dd.MM.yyyy.");
        }

        private void cmb_Knjiga_SelectedValueChanged(object sender, EventArgs e)
        {
            cmb_Primerak.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiPrimerke((Knjiga)cmb_Knjiga.SelectedItem);
        }

        private void btn_SacuvajPozajmicu_Click(object sender, EventArgs e)
        {
           
                    if (!DateTime.TryParseExact(txt_DatumKreiranjaPozajmice.Text, "dd.MM.yyyy.", null, System.Globalization.DateTimeStyles.None, out DateTime datumKreiranjaPozajmice))
                    {
                        MessageBox.Show("Datum nije dobro unet.");
                        MessageBox.Show("Sistem ne moze da kreira pozajmicu.");
            }
                    else
                    {
                        if(cmb_Primerak.SelectedItem != null)
                        {
                    KontrolerKorisnickogInterfejsa.Instance.SacuvajPozajmicu(new Pozajmica()
                    {
                        PrimerakKnjige = (PrimerakKnjige)cmb_Primerak.SelectedItem,
                        Korisnik = (Korisnik)cmb_Korisnik.SelectedItem,
                        DatumKreiranjaPozajmice = datumKreiranjaPozajmice,
                        Aktivna = chb_Aktivna.Checked
                    });
                    MessageBox.Show("Sistem je uspesno kreira pozajmicu.");
                }
                    else
                    {
                        MessageBox.Show("Odabrana knjiga nema nijedan slobodan primerak.");
                        MessageBox.Show("Sistem ne moze da kreira pozajmicu.");
                }
                    }
            
        }
    }
}
