﻿namespace Forme
{
    partial class FrmPozajmice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_PretraziKorisnika = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_Pozajmice = new System.Windows.Forms.DataGridView();
            this.btn_DeaktivirajPozajmicu = new System.Windows.Forms.Button();
            this.btn_KreirajPozajmicu = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.korisniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.knjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Pozajmice)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbl_PretraziKorisnika
            // 
            this.lbl_PretraziKorisnika.AutoSize = true;
            this.lbl_PretraziKorisnika.Location = new System.Drawing.Point(-43, 56);
            this.lbl_PretraziKorisnika.Name = "lbl_PretraziKorisnika";
            this.lbl_PretraziKorisnika.Size = new System.Drawing.Size(45, 13);
            this.lbl_PretraziKorisnika.TabIndex = 17;
            this.lbl_PretraziKorisnika.Text = "Pretrazi:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_Pozajmice);
            this.groupBox1.Location = new System.Drawing.Point(18, 93);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(715, 249);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pozajmice";
            // 
            // dgv_Pozajmice
            // 
            this.dgv_Pozajmice.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Pozajmice.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Pozajmice.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Pozajmice.Location = new System.Drawing.Point(6, 18);
            this.dgv_Pozajmice.Name = "dgv_Pozajmice";
            this.dgv_Pozajmice.ReadOnly = true;
            this.dgv_Pozajmice.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Pozajmice.Size = new System.Drawing.Size(697, 224);
            this.dgv_Pozajmice.TabIndex = 2;
            // 
            // btn_DeaktivirajPozajmicu
            // 
            this.btn_DeaktivirajPozajmicu.Location = new System.Drawing.Point(396, 51);
            this.btn_DeaktivirajPozajmicu.Name = "btn_DeaktivirajPozajmicu";
            this.btn_DeaktivirajPozajmicu.Size = new System.Drawing.Size(325, 36);
            this.btn_DeaktivirajPozajmicu.TabIndex = 24;
            this.btn_DeaktivirajPozajmicu.Text = "Deaktiviraj pozajmicu";
            this.btn_DeaktivirajPozajmicu.UseVisualStyleBackColor = true;
            this.btn_DeaktivirajPozajmicu.Click += new System.EventHandler(this.btn_DeaktivirajPozajmicu_Click);
            // 
            // btn_KreirajPozajmicu
            // 
            this.btn_KreirajPozajmicu.Location = new System.Drawing.Point(24, 51);
            this.btn_KreirajPozajmicu.Name = "btn_KreirajPozajmicu";
            this.btn_KreirajPozajmicu.Size = new System.Drawing.Size(320, 36);
            this.btn_KreirajPozajmicu.TabIndex = 20;
            this.btn_KreirajPozajmicu.Text = "Kreiraj pozajmicu";
            this.btn_KreirajPozajmicu.UseVisualStyleBackColor = true;
            this.btn_KreirajPozajmicu.Click += new System.EventHandler(this.btn_KreirajPozajmicu_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.korisniciToolStripMenuItem,
            this.knjigeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(767, 24);
            this.menuStrip1.TabIndex = 25;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // korisniciToolStripMenuItem
            // 
            this.korisniciToolStripMenuItem.Name = "korisniciToolStripMenuItem";
            this.korisniciToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.korisniciToolStripMenuItem.Text = "Korisnici";
            this.korisniciToolStripMenuItem.Click += new System.EventHandler(this.korisniciToolStripMenuItem_Click);
            // 
            // knjigeToolStripMenuItem
            // 
            this.knjigeToolStripMenuItem.Name = "knjigeToolStripMenuItem";
            this.knjigeToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.knjigeToolStripMenuItem.Text = "Knjige";
            this.knjigeToolStripMenuItem.Click += new System.EventHandler(this.knjigeToolStripMenuItem_Click);
            // 
            // FrmPozajmice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(767, 381);
            this.Controls.Add(this.btn_DeaktivirajPozajmicu);
            this.Controls.Add(this.btn_KreirajPozajmicu);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lbl_PretraziKorisnika);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmPozajmice";
            this.Text = "FrmPozajmice";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrmPozajmice_FormClosed);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Pozajmice)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lbl_PretraziKorisnika;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_Pozajmice;
        private System.Windows.Forms.Button btn_DeaktivirajPozajmicu;
        private System.Windows.Forms.Button btn_KreirajPozajmicu;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem korisniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem knjigeToolStripMenuItem;
    }
}