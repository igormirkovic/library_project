﻿namespace Forme
{
    partial class FrmKreirajPromeniKorisnika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_SacuvajKorisnika = new System.Windows.Forms.Button();
            this.lbl_Pol = new System.Windows.Forms.Label();
            this.cmb_Pol = new System.Windows.Forms.ComboBox();
            this.lbl_DatumRodjenja = new System.Windows.Forms.Label();
            this.lbl_ImePrezime = new System.Windows.Forms.Label();
            this.txt_ImePrezime = new System.Windows.Forms.TextBox();
            this.lbl_StrucnaSprema = new System.Windows.Forms.Label();
            this.cmb_StrucnaSprema = new System.Windows.Forms.ComboBox();
            this.txt_DatumRodjenja = new System.Windows.Forms.TextBox();
            this.chb_Aktivan = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btn_SacuvajKorisnika
            // 
            this.btn_SacuvajKorisnika.Location = new System.Drawing.Point(104, 205);
            this.btn_SacuvajKorisnika.Name = "btn_SacuvajKorisnika";
            this.btn_SacuvajKorisnika.Size = new System.Drawing.Size(156, 50);
            this.btn_SacuvajKorisnika.TabIndex = 15;
            this.btn_SacuvajKorisnika.Text = "Sacuvaj korisnika";
            this.btn_SacuvajKorisnika.UseVisualStyleBackColor = true;
            this.btn_SacuvajKorisnika.Click += new System.EventHandler(this.btn_SacuvajKorisnika_Click);
            // 
            // lbl_Pol
            // 
            this.lbl_Pol.AutoSize = true;
            this.lbl_Pol.Location = new System.Drawing.Point(47, 92);
            this.lbl_Pol.Name = "lbl_Pol";
            this.lbl_Pol.Size = new System.Drawing.Size(25, 13);
            this.lbl_Pol.TabIndex = 13;
            this.lbl_Pol.Text = "Pol:";
            // 
            // cmb_Pol
            // 
            this.cmb_Pol.FormattingEnabled = true;
            this.cmb_Pol.Items.AddRange(new object[] {
            "Muski",
            "Zenski"});
            this.cmb_Pol.Location = new System.Drawing.Point(104, 89);
            this.cmb_Pol.Name = "cmb_Pol";
            this.cmb_Pol.Size = new System.Drawing.Size(156, 21);
            this.cmb_Pol.TabIndex = 12;
            // 
            // lbl_DatumRodjenja
            // 
            this.lbl_DatumRodjenja.AutoSize = true;
            this.lbl_DatumRodjenja.Location = new System.Drawing.Point(15, 59);
            this.lbl_DatumRodjenja.Name = "lbl_DatumRodjenja";
            this.lbl_DatumRodjenja.Size = new System.Drawing.Size(83, 13);
            this.lbl_DatumRodjenja.TabIndex = 10;
            this.lbl_DatumRodjenja.Text = "DatumRodjenja:";
            // 
            // lbl_ImePrezime
            // 
            this.lbl_ImePrezime.AutoSize = true;
            this.lbl_ImePrezime.Location = new System.Drawing.Point(15, 26);
            this.lbl_ImePrezime.Name = "lbl_ImePrezime";
            this.lbl_ImePrezime.Size = new System.Drawing.Size(74, 13);
            this.lbl_ImePrezime.TabIndex = 9;
            this.lbl_ImePrezime.Text = "Ime i prezime: ";
            // 
            // txt_ImePrezime
            // 
            this.txt_ImePrezime.Location = new System.Drawing.Point(104, 23);
            this.txt_ImePrezime.Name = "txt_ImePrezime";
            this.txt_ImePrezime.Size = new System.Drawing.Size(156, 20);
            this.txt_ImePrezime.TabIndex = 8;
            // 
            // lbl_StrucnaSprema
            // 
            this.lbl_StrucnaSprema.AutoSize = true;
            this.lbl_StrucnaSprema.Location = new System.Drawing.Point(15, 124);
            this.lbl_StrucnaSprema.Name = "lbl_StrucnaSprema";
            this.lbl_StrucnaSprema.Size = new System.Drawing.Size(87, 13);
            this.lbl_StrucnaSprema.TabIndex = 16;
            this.lbl_StrucnaSprema.Text = "Strucna sprema: ";
            // 
            // cmb_StrucnaSprema
            // 
            this.cmb_StrucnaSprema.FormattingEnabled = true;
            this.cmb_StrucnaSprema.Items.AddRange(new object[] {
            "OsnovaSkola",
            "SrednjaSkola",
            "VisaSkola",
            "Fakultet",
            "MasterDiploma",
            "DoktorDiploma"});
            this.cmb_StrucnaSprema.Location = new System.Drawing.Point(104, 121);
            this.cmb_StrucnaSprema.Name = "cmb_StrucnaSprema";
            this.cmb_StrucnaSprema.Size = new System.Drawing.Size(156, 21);
            this.cmb_StrucnaSprema.TabIndex = 17;
            // 
            // txt_DatumRodjenja
            // 
            this.txt_DatumRodjenja.Location = new System.Drawing.Point(104, 56);
            this.txt_DatumRodjenja.Name = "txt_DatumRodjenja";
            this.txt_DatumRodjenja.Size = new System.Drawing.Size(156, 20);
            this.txt_DatumRodjenja.TabIndex = 18;
            // 
            // chb_Aktivan
            // 
            this.chb_Aktivan.AutoSize = true;
            this.chb_Aktivan.Checked = true;
            this.chb_Aktivan.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chb_Aktivan.Location = new System.Drawing.Point(104, 163);
            this.chb_Aktivan.Name = "chb_Aktivan";
            this.chb_Aktivan.Size = new System.Drawing.Size(62, 17);
            this.chb_Aktivan.TabIndex = 19;
            this.chb_Aktivan.Text = "Aktivan";
            this.chb_Aktivan.UseVisualStyleBackColor = true;
            // 
            // FrmKreirajPromeniKorisnika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 289);
            this.Controls.Add(this.chb_Aktivan);
            this.Controls.Add(this.txt_DatumRodjenja);
            this.Controls.Add(this.cmb_StrucnaSprema);
            this.Controls.Add(this.lbl_StrucnaSprema);
            this.Controls.Add(this.btn_SacuvajKorisnika);
            this.Controls.Add(this.lbl_Pol);
            this.Controls.Add(this.cmb_Pol);
            this.Controls.Add(this.lbl_DatumRodjenja);
            this.Controls.Add(this.lbl_ImePrezime);
            this.Controls.Add(this.txt_ImePrezime);
            this.Name = "FrmKreirajPromeniKorisnika";
            this.Text = "KreirajPromeniKorisnika";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_SacuvajKorisnika;
        private System.Windows.Forms.Label lbl_Pol;
        private System.Windows.Forms.ComboBox cmb_Pol;
        private System.Windows.Forms.Label lbl_DatumRodjenja;
        private System.Windows.Forms.Label lbl_ImePrezime;
        private System.Windows.Forms.TextBox txt_ImePrezime;
        private System.Windows.Forms.Label lbl_StrucnaSprema;
        private System.Windows.Forms.ComboBox cmb_StrucnaSprema;
        private System.Windows.Forms.TextBox txt_DatumRodjenja;
        private System.Windows.Forms.CheckBox chb_Aktivan;
    }
}