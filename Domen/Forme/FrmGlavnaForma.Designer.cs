﻿namespace Forme
{
    partial class FrmGlavnaForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.korisniciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.knjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_Knjige = new System.Windows.Forms.DataGridView();
            this.btn_KreirajKnjigu = new System.Windows.Forms.Button();
            this.btn_DeaktivirajKnjigu = new System.Windows.Forms.Button();
            this.lbl_PretraziKnjigu = new System.Windows.Forms.Label();
            this.txt_PretraziKnjigu = new System.Windows.Forms.TextBox();
            this.btn_PretraziKnjigu = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Knjige)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.korisniciToolStripMenuItem,
            this.knjigeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(668, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // korisniciToolStripMenuItem
            // 
            this.korisniciToolStripMenuItem.Name = "korisniciToolStripMenuItem";
            this.korisniciToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
            this.korisniciToolStripMenuItem.Text = "Korisnici";
            this.korisniciToolStripMenuItem.Click += new System.EventHandler(this.korisniciToolStripMenuItem_Click);
            // 
            // knjigeToolStripMenuItem
            // 
            this.knjigeToolStripMenuItem.Name = "knjigeToolStripMenuItem";
            this.knjigeToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.knjigeToolStripMenuItem.Text = "Pozajmice";
            this.knjigeToolStripMenuItem.Click += new System.EventHandler(this.knjigeToolStripMenuItem_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_Knjige);
            this.groupBox1.Location = new System.Drawing.Point(12, 83);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(644, 249);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Knjige";
            // 
            // dgv_Knjige
            // 
            this.dgv_Knjige.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Knjige.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Knjige.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Knjige.Location = new System.Drawing.Point(0, 19);
            this.dgv_Knjige.Name = "dgv_Knjige";
            this.dgv_Knjige.ReadOnly = true;
            this.dgv_Knjige.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Knjige.Size = new System.Drawing.Size(644, 224);
            this.dgv_Knjige.TabIndex = 2;
            // 
            // btn_KreirajKnjigu
            // 
            this.btn_KreirajKnjigu.Location = new System.Drawing.Point(340, 49);
            this.btn_KreirajKnjigu.Name = "btn_KreirajKnjigu";
            this.btn_KreirajKnjigu.Size = new System.Drawing.Size(155, 23);
            this.btn_KreirajKnjigu.TabIndex = 2;
            this.btn_KreirajKnjigu.Text = "Kreiraj knjigu";
            this.btn_KreirajKnjigu.UseVisualStyleBackColor = true;
            this.btn_KreirajKnjigu.Click += new System.EventHandler(this.btn_KreirajKnjigu_Click);
            // 
            // btn_DeaktivirajKnjigu
            // 
            this.btn_DeaktivirajKnjigu.Location = new System.Drawing.Point(501, 49);
            this.btn_DeaktivirajKnjigu.Name = "btn_DeaktivirajKnjigu";
            this.btn_DeaktivirajKnjigu.Size = new System.Drawing.Size(155, 23);
            this.btn_DeaktivirajKnjigu.TabIndex = 3;
            this.btn_DeaktivirajKnjigu.Text = "Deaktiviraj Knjigu";
            this.btn_DeaktivirajKnjigu.UseVisualStyleBackColor = true;
            this.btn_DeaktivirajKnjigu.Click += new System.EventHandler(this.btn_DeaktivirajKnjigu_Click);
            // 
            // lbl_PretraziKnjigu
            // 
            this.lbl_PretraziKnjigu.AutoSize = true;
            this.lbl_PretraziKnjigu.Location = new System.Drawing.Point(12, 54);
            this.lbl_PretraziKnjigu.Name = "lbl_PretraziKnjigu";
            this.lbl_PretraziKnjigu.Size = new System.Drawing.Size(45, 13);
            this.lbl_PretraziKnjigu.TabIndex = 4;
            this.lbl_PretraziKnjigu.Text = "Pretrazi:";
            // 
            // txt_PretraziKnjigu
            // 
            this.txt_PretraziKnjigu.Location = new System.Drawing.Point(63, 51);
            this.txt_PretraziKnjigu.Name = "txt_PretraziKnjigu";
            this.txt_PretraziKnjigu.Size = new System.Drawing.Size(125, 20);
            this.txt_PretraziKnjigu.TabIndex = 5;
            // 
            // btn_PretraziKnjigu
            // 
            this.btn_PretraziKnjigu.Location = new System.Drawing.Point(204, 48);
            this.btn_PretraziKnjigu.Name = "btn_PretraziKnjigu";
            this.btn_PretraziKnjigu.Size = new System.Drawing.Size(79, 23);
            this.btn_PretraziKnjigu.TabIndex = 6;
            this.btn_PretraziKnjigu.Text = "Pretrazi";
            this.btn_PretraziKnjigu.UseVisualStyleBackColor = true;
            this.btn_PretraziKnjigu.Click += new System.EventHandler(this.btn_PretraziKnjigu_Click);
            // 
            // FrmGlavnaForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(668, 361);
            this.Controls.Add(this.btn_PretraziKnjigu);
            this.Controls.Add(this.txt_PretraziKnjigu);
            this.Controls.Add(this.lbl_PretraziKnjigu);
            this.Controls.Add(this.btn_DeaktivirajKnjigu);
            this.Controls.Add(this.btn_KreirajKnjigu);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmGlavnaForma";
            this.Text = "FrmGlavnaForma";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Knjige)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem korisniciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem knjigeToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_Knjige;
        private System.Windows.Forms.Button btn_KreirajKnjigu;
        private System.Windows.Forms.Button btn_DeaktivirajKnjigu;
        private System.Windows.Forms.Label lbl_PretraziKnjigu;
        private System.Windows.Forms.TextBox txt_PretraziKnjigu;
        private System.Windows.Forms.Button btn_PretraziKnjigu;
    }
}