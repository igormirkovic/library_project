﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forme
{
    public partial class FrmGlavnaForma : Form
    {
        KontrolerKorisnickogInterfejsa kontroler = KontrolerKorisnickogInterfejsa.Instance;
        bool kliknutoDugmePretrazi = false;
        public FrmGlavnaForma()
        {
            InitializeComponent();
            this.Text = $"Dobro dosli, {Sesija.Instance.Bibliotekar.ImePrezime}";
            dgv_Knjige.DataSource = kontroler.VratiSveKnjige();
            Timer t;
            t = new Timer();
            t.Interval = 5000;
            t.Tick += prikaziKnjige;
            t.Start();
            
        }

        private void prikaziKnjige(object sender, EventArgs e)
        {
           if (!kliknutoDugmePretrazi)
            {
                dgv_Knjige.DataSource = kontroler.VratiSveKnjige();
            }
            else
            {
                dgv_Knjige.DataSource = kontroler.PretraziKnjigu(txt_PretraziKnjigu.Text);
            }
        }

        private void btn_KreirajKnjigu_Click(object sender, EventArgs e)
        {
            new FrmKreirajKnjigu().ShowDialog();
        }

        private void btn_PretraziKnjigu_Click(object sender, EventArgs e)
        {
            kliknutoDugmePretrazi = true;
            dgv_Knjige.DataSource = kontroler.PretraziKnjigu(txt_PretraziKnjigu.Text);
            if(kontroler.PretraziKnjigu(txt_PretraziKnjigu.Text).Count == 0)
            {
                MessageBox.Show("Sistem ne moze da nadje knjigu po zadatoj vrednosti.");
            }
        }

        private void btn_DeaktivirajKnjigu_Click(object sender, EventArgs e)
        {
            try
            {
                if (kontroler.DeaktivirajKnjigu((Knjiga)dgv_Knjige.SelectedRows[0].DataBoundItem))
                {
                    MessageBox.Show("Sistem je uspesno deaktivirao knjigu.");
                    prikaziKnjige(sender, e);

                }
                else
                {
                    MessageBox.Show("Sistem ne moze da deaktivira knjigu.");
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Niste odabrali ni jednu knjigu.");
                MessageBox.Show("Sistem ne moze da deaktivira knjigu.");
            }
        }

       

        private void korisniciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FrmKorisnik().ShowDialog();
            this.Close();
        }

        private void knjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FrmPozajmice().ShowDialog();
            this.Close();
        }
    }
}
