﻿namespace Forme
{
    partial class FrmKreiranjePozajmice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chb_Aktivna = new System.Windows.Forms.CheckBox();
            this.lbl_StrucnaSprema = new System.Windows.Forms.Label();
            this.btn_SacuvajPozajmicu = new System.Windows.Forms.Button();
            this.lbl_Primerak = new System.Windows.Forms.Label();
            this.cmb_Primerak = new System.Windows.Forms.ComboBox();
            this.lbl_DatumRodjenja = new System.Windows.Forms.Label();
            this.lbl_Korisnik = new System.Windows.Forms.Label();
            this.cmb_Korisnik = new System.Windows.Forms.ComboBox();
            this.cmb_Knjiga = new System.Windows.Forms.ComboBox();
            this.txt_DatumKreiranjaPozajmice = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // chb_Aktivna
            // 
            this.chb_Aktivna.AutoSize = true;
            this.chb_Aktivna.Checked = true;
            this.chb_Aktivna.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chb_Aktivna.Location = new System.Drawing.Point(112, 151);
            this.chb_Aktivna.Name = "chb_Aktivna";
            this.chb_Aktivna.Size = new System.Drawing.Size(62, 17);
            this.chb_Aktivna.TabIndex = 29;
            this.chb_Aktivna.Text = "Aktivan";
            this.chb_Aktivna.UseVisualStyleBackColor = true;
            // 
            // lbl_StrucnaSprema
            // 
            this.lbl_StrucnaSprema.AutoSize = true;
            this.lbl_StrucnaSprema.Location = new System.Drawing.Point(21, 113);
            this.lbl_StrucnaSprema.Name = "lbl_StrucnaSprema";
            this.lbl_StrucnaSprema.Size = new System.Drawing.Size(85, 13);
            this.lbl_StrucnaSprema.TabIndex = 26;
            this.lbl_StrucnaSprema.Text = "DatumKreiranja: ";
            // 
            // btn_SacuvajPozajmicu
            // 
            this.btn_SacuvajPozajmicu.Location = new System.Drawing.Point(112, 186);
            this.btn_SacuvajPozajmicu.Name = "btn_SacuvajPozajmicu";
            this.btn_SacuvajPozajmicu.Size = new System.Drawing.Size(156, 50);
            this.btn_SacuvajPozajmicu.TabIndex = 25;
            this.btn_SacuvajPozajmicu.Text = "Sacuvaj pozajmicu";
            this.btn_SacuvajPozajmicu.UseVisualStyleBackColor = true;
            this.btn_SacuvajPozajmicu.Click += new System.EventHandler(this.btn_SacuvajPozajmicu_Click);
            // 
            // lbl_Primerak
            // 
            this.lbl_Primerak.AutoSize = true;
            this.lbl_Primerak.Location = new System.Drawing.Point(23, 81);
            this.lbl_Primerak.Name = "lbl_Primerak";
            this.lbl_Primerak.Size = new System.Drawing.Size(51, 13);
            this.lbl_Primerak.TabIndex = 24;
            this.lbl_Primerak.Text = "Primerak:";
            // 
            // cmb_Primerak
            // 
            this.cmb_Primerak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Primerak.FormattingEnabled = true;
            this.cmb_Primerak.Location = new System.Drawing.Point(112, 78);
            this.cmb_Primerak.Name = "cmb_Primerak";
            this.cmb_Primerak.Size = new System.Drawing.Size(156, 21);
            this.cmb_Primerak.TabIndex = 23;
            // 
            // lbl_DatumRodjenja
            // 
            this.lbl_DatumRodjenja.AutoSize = true;
            this.lbl_DatumRodjenja.Location = new System.Drawing.Point(23, 48);
            this.lbl_DatumRodjenja.Name = "lbl_DatumRodjenja";
            this.lbl_DatumRodjenja.Size = new System.Drawing.Size(39, 13);
            this.lbl_DatumRodjenja.TabIndex = 22;
            this.lbl_DatumRodjenja.Text = "Knjiga:";
            // 
            // lbl_Korisnik
            // 
            this.lbl_Korisnik.AutoSize = true;
            this.lbl_Korisnik.Location = new System.Drawing.Point(23, 15);
            this.lbl_Korisnik.Name = "lbl_Korisnik";
            this.lbl_Korisnik.Size = new System.Drawing.Size(47, 13);
            this.lbl_Korisnik.TabIndex = 21;
            this.lbl_Korisnik.Text = "Korisnik:";
            // 
            // cmb_Korisnik
            // 
            this.cmb_Korisnik.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Korisnik.FormattingEnabled = true;
            this.cmb_Korisnik.Location = new System.Drawing.Point(112, 12);
            this.cmb_Korisnik.Name = "cmb_Korisnik";
            this.cmb_Korisnik.Size = new System.Drawing.Size(156, 21);
            this.cmb_Korisnik.TabIndex = 30;
            // 
            // cmb_Knjiga
            // 
            this.cmb_Knjiga.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Knjiga.FormattingEnabled = true;
            this.cmb_Knjiga.Location = new System.Drawing.Point(112, 45);
            this.cmb_Knjiga.Name = "cmb_Knjiga";
            this.cmb_Knjiga.Size = new System.Drawing.Size(156, 21);
            this.cmb_Knjiga.TabIndex = 31;
            this.cmb_Knjiga.SelectedValueChanged += new System.EventHandler(this.cmb_Knjiga_SelectedValueChanged);
            // 
            // txt_DatumKreiranjaPozajmice
            // 
            this.txt_DatumKreiranjaPozajmice.Location = new System.Drawing.Point(112, 110);
            this.txt_DatumKreiranjaPozajmice.Name = "txt_DatumKreiranjaPozajmice";
            this.txt_DatumKreiranjaPozajmice.Size = new System.Drawing.Size(156, 20);
            this.txt_DatumKreiranjaPozajmice.TabIndex = 32;
            // 
            // FrmKreiranjePozajmice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(344, 257);
            this.Controls.Add(this.txt_DatumKreiranjaPozajmice);
            this.Controls.Add(this.cmb_Knjiga);
            this.Controls.Add(this.cmb_Korisnik);
            this.Controls.Add(this.chb_Aktivna);
            this.Controls.Add(this.lbl_StrucnaSprema);
            this.Controls.Add(this.btn_SacuvajPozajmicu);
            this.Controls.Add(this.lbl_Primerak);
            this.Controls.Add(this.cmb_Primerak);
            this.Controls.Add(this.lbl_DatumRodjenja);
            this.Controls.Add(this.lbl_Korisnik);
            this.Name = "FrmKreiranjePozajmice";
            this.Text = "FrmKreiranjePozajmice";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chb_Aktivna;
        private System.Windows.Forms.Label lbl_StrucnaSprema;
        private System.Windows.Forms.Button btn_SacuvajPozajmicu;
        private System.Windows.Forms.Label lbl_Primerak;
        private System.Windows.Forms.ComboBox cmb_Primerak;
        private System.Windows.Forms.Label lbl_DatumRodjenja;
        private System.Windows.Forms.Label lbl_Korisnik;
        private System.Windows.Forms.ComboBox cmb_Korisnik;
        private System.Windows.Forms.ComboBox cmb_Knjiga;
        private System.Windows.Forms.TextBox txt_DatumKreiranjaPozajmice;
    }
}