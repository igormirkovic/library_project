﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forme
{
    public partial class FrmPozajmice : Form
    {
        public FrmPozajmice()
        {
            InitializeComponent();
            dgv_Pozajmice.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiPozajmice();
            Timer t;
            t = new Timer();
            t.Interval = 5000;
            t.Tick += prikaziPozajmice;
            t.Start();
        }

        private void btn_KreirajPozajmicu_Click(object sender, EventArgs e)
        {
            new FrmKreiranjePozajmice().ShowDialog();
        }

        private void prikaziPozajmice(object sender, EventArgs e)
        {
            dgv_Pozajmice.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiPozajmice();
        }

        private void btn_DeaktivirajPozajmicu_Click(object sender, EventArgs e)
        {
            try
            {
                if (KontrolerKorisnickogInterfejsa.Instance.DeaktivirajPozajmicu((Pozajmica)dgv_Pozajmice.SelectedRows[0].DataBoundItem))
                {
                    MessageBox.Show("Sistem je uspesno deaktivirao pozajmicu.");
                }
                else
                {
                    MessageBox.Show("Sistem ne moze da deaktivira pozajmicu.");
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Niste odabrali nijednu pozajmicu.");
            }
        }

        private void korisniciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FrmKorisnik().ShowDialog();
            this.Close();
        }

        private void knjigeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Hide();
            new FrmGlavnaForma().ShowDialog();
            this.Close();
        }

        private void FrmPozajmice_FormClosed(object sender, FormClosedEventArgs e)
        {
            KontrolerKorisnickogInterfejsa.Instance.Kraj();
        }
    }
}
