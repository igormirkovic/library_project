﻿namespace Forme
{
    partial class FrmKreirajKnjigu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_NazivKnjige = new System.Windows.Forms.TextBox();
            this.lbl_NazivKnjige = new System.Windows.Forms.Label();
            this.lbl_Autor = new System.Windows.Forms.Label();
            this.cmb_Autor = new System.Windows.Forms.ComboBox();
            this.cmb_Zanr = new System.Windows.Forms.ComboBox();
            this.lbl_Zanr = new System.Windows.Forms.Label();
            this.chb_Aktivna = new System.Windows.Forms.CheckBox();
            this.btn_SacuvajKnjigu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_NazivKnjige
            // 
            this.txt_NazivKnjige.Location = new System.Drawing.Point(129, 28);
            this.txt_NazivKnjige.Name = "txt_NazivKnjige";
            this.txt_NazivKnjige.Size = new System.Drawing.Size(156, 20);
            this.txt_NazivKnjige.TabIndex = 0;
            // 
            // lbl_NazivKnjige
            // 
            this.lbl_NazivKnjige.AutoSize = true;
            this.lbl_NazivKnjige.Location = new System.Drawing.Point(39, 31);
            this.lbl_NazivKnjige.Name = "lbl_NazivKnjige";
            this.lbl_NazivKnjige.Size = new System.Drawing.Size(68, 13);
            this.lbl_NazivKnjige.TabIndex = 1;
            this.lbl_NazivKnjige.Text = "Naziv knjige:";
            // 
            // lbl_Autor
            // 
            this.lbl_Autor.AutoSize = true;
            this.lbl_Autor.Location = new System.Drawing.Point(72, 64);
            this.lbl_Autor.Name = "lbl_Autor";
            this.lbl_Autor.Size = new System.Drawing.Size(35, 13);
            this.lbl_Autor.TabIndex = 2;
            this.lbl_Autor.Text = "Autor:";
            // 
            // cmb_Autor
            // 
            this.cmb_Autor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Autor.FormattingEnabled = true;
            this.cmb_Autor.Location = new System.Drawing.Point(129, 61);
            this.cmb_Autor.Name = "cmb_Autor";
            this.cmb_Autor.Size = new System.Drawing.Size(156, 21);
            this.cmb_Autor.TabIndex = 3;
            // 
            // cmb_Zanr
            // 
            this.cmb_Zanr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Zanr.FormattingEnabled = true;
            this.cmb_Zanr.Location = new System.Drawing.Point(129, 99);
            this.cmb_Zanr.Name = "cmb_Zanr";
            this.cmb_Zanr.Size = new System.Drawing.Size(156, 21);
            this.cmb_Zanr.TabIndex = 4;
            // 
            // lbl_Zanr
            // 
            this.lbl_Zanr.AutoSize = true;
            this.lbl_Zanr.Location = new System.Drawing.Point(72, 102);
            this.lbl_Zanr.Name = "lbl_Zanr";
            this.lbl_Zanr.Size = new System.Drawing.Size(32, 13);
            this.lbl_Zanr.TabIndex = 5;
            this.lbl_Zanr.Text = "Zanr:";
            // 
            // chb_Aktivna
            // 
            this.chb_Aktivna.AutoSize = true;
            this.chb_Aktivna.Location = new System.Drawing.Point(75, 135);
            this.chb_Aktivna.Name = "chb_Aktivna";
            this.chb_Aktivna.Size = new System.Drawing.Size(62, 17);
            this.chb_Aktivna.TabIndex = 6;
            this.chb_Aktivna.Text = "Aktivna";
            this.chb_Aktivna.UseVisualStyleBackColor = true;
            // 
            // btn_SacuvajKnjigu
            // 
            this.btn_SacuvajKnjigu.Location = new System.Drawing.Point(152, 167);
            this.btn_SacuvajKnjigu.Name = "btn_SacuvajKnjigu";
            this.btn_SacuvajKnjigu.Size = new System.Drawing.Size(133, 50);
            this.btn_SacuvajKnjigu.TabIndex = 7;
            this.btn_SacuvajKnjigu.Text = "Sacuvaj knjigu";
            this.btn_SacuvajKnjigu.UseVisualStyleBackColor = true;
            this.btn_SacuvajKnjigu.Click += new System.EventHandler(this.btn_SacuvajKnjigu_Click);
            // 
            // FrmKreirajKnjigu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 252);
            this.Controls.Add(this.btn_SacuvajKnjigu);
            this.Controls.Add(this.chb_Aktivna);
            this.Controls.Add(this.lbl_Zanr);
            this.Controls.Add(this.cmb_Zanr);
            this.Controls.Add(this.cmb_Autor);
            this.Controls.Add(this.lbl_Autor);
            this.Controls.Add(this.lbl_NazivKnjige);
            this.Controls.Add(this.txt_NazivKnjige);
            this.Name = "FrmKreirajKnjigu";
            this.Text = "FrmKnjiga";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_NazivKnjige;
        private System.Windows.Forms.Label lbl_NazivKnjige;
        private System.Windows.Forms.Label lbl_Autor;
        private System.Windows.Forms.ComboBox cmb_Autor;
        private System.Windows.Forms.ComboBox cmb_Zanr;
        private System.Windows.Forms.Label lbl_Zanr;
        private System.Windows.Forms.CheckBox chb_Aktivna;
        private System.Windows.Forms.Button btn_SacuvajKnjigu;
    }
}