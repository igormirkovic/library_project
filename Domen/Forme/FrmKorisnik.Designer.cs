﻿namespace Forme
{
    partial class FrmKorisnik
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_PretraziKorisnika = new System.Windows.Forms.Button();
            this.txt_PretraziKorisnika = new System.Windows.Forms.TextBox();
            this.lbl_PretraziKorisnika = new System.Windows.Forms.Label();
            this.btn_KreirajKorisnika = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dgv_Korisnici = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.knjigeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.knjigeToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.btn_PromeniKorisnika = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Korisnici)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_PretraziKorisnika
            // 
            this.btn_PretraziKorisnika.Location = new System.Drawing.Point(227, 54);
            this.btn_PretraziKorisnika.Name = "btn_PretraziKorisnika";
            this.btn_PretraziKorisnika.Size = new System.Drawing.Size(79, 23);
            this.btn_PretraziKorisnika.TabIndex = 13;
            this.btn_PretraziKorisnika.Text = "Pretrazi";
            this.btn_PretraziKorisnika.UseVisualStyleBackColor = true;
            this.btn_PretraziKorisnika.Click += new System.EventHandler(this.btn_PretraziKorisnika_Click);
            // 
            // txt_PretraziKorisnika
            // 
            this.txt_PretraziKorisnika.Location = new System.Drawing.Point(82, 56);
            this.txt_PretraziKorisnika.Name = "txt_PretraziKorisnika";
            this.txt_PretraziKorisnika.Size = new System.Drawing.Size(125, 20);
            this.txt_PretraziKorisnika.TabIndex = 12;
            // 
            // lbl_PretraziKorisnika
            // 
            this.lbl_PretraziKorisnika.AutoSize = true;
            this.lbl_PretraziKorisnika.Location = new System.Drawing.Point(15, 59);
            this.lbl_PretraziKorisnika.Name = "lbl_PretraziKorisnika";
            this.lbl_PretraziKorisnika.Size = new System.Drawing.Size(45, 13);
            this.lbl_PretraziKorisnika.TabIndex = 11;
            this.lbl_PretraziKorisnika.Text = "Pretrazi:";
            // 
            // btn_KreirajKorisnika
            // 
            this.btn_KreirajKorisnika.Location = new System.Drawing.Point(352, 54);
            this.btn_KreirajKorisnika.Name = "btn_KreirajKorisnika";
            this.btn_KreirajKorisnika.Size = new System.Drawing.Size(162, 23);
            this.btn_KreirajKorisnika.TabIndex = 9;
            this.btn_KreirajKorisnika.Text = "Kreiraj korisnika";
            this.btn_KreirajKorisnika.UseVisualStyleBackColor = true;
            this.btn_KreirajKorisnika.Click += new System.EventHandler(this.btn_KreirajKorisnika_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_Korisnici);
            this.groupBox1.Location = new System.Drawing.Point(12, 83);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(715, 249);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Korisnici";
            // 
            // dgv_Korisnici
            // 
            this.dgv_Korisnici.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_Korisnici.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.dgv_Korisnici.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_Korisnici.Location = new System.Drawing.Point(6, 18);
            this.dgv_Korisnici.Name = "dgv_Korisnici";
            this.dgv_Korisnici.ReadOnly = true;
            this.dgv_Korisnici.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dgv_Korisnici.Size = new System.Drawing.Size(703, 224);
            this.dgv_Korisnici.TabIndex = 2;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.knjigeToolStripMenuItem,
            this.knjigeToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(727, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // knjigeToolStripMenuItem
            // 
            this.knjigeToolStripMenuItem.Name = "knjigeToolStripMenuItem";
            this.knjigeToolStripMenuItem.Size = new System.Drawing.Size(73, 20);
            this.knjigeToolStripMenuItem.Text = "Pozajmice";
            this.knjigeToolStripMenuItem.Click += new System.EventHandler(this.knjigeToolStripMenuItem_Click);
            // 
            // knjigeToolStripMenuItem1
            // 
            this.knjigeToolStripMenuItem1.Name = "knjigeToolStripMenuItem1";
            this.knjigeToolStripMenuItem1.Size = new System.Drawing.Size(52, 20);
            this.knjigeToolStripMenuItem1.Text = "Knjige";
            this.knjigeToolStripMenuItem1.Click += new System.EventHandler(this.knjigeToolStripMenuItem1_Click);
            // 
            // btn_PromeniKorisnika
            // 
            this.btn_PromeniKorisnika.Location = new System.Drawing.Point(545, 54);
            this.btn_PromeniKorisnika.Name = "btn_PromeniKorisnika";
            this.btn_PromeniKorisnika.Size = new System.Drawing.Size(159, 23);
            this.btn_PromeniKorisnika.TabIndex = 14;
            this.btn_PromeniKorisnika.Text = "Promeni korisnika";
            this.btn_PromeniKorisnika.UseVisualStyleBackColor = true;
            this.btn_PromeniKorisnika.Click += new System.EventHandler(this.btn_PromeniKorisnika_Click);
            // 
            // FrmKorisnik
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(727, 342);
            this.Controls.Add(this.btn_PromeniKorisnika);
            this.Controls.Add(this.btn_PretraziKorisnika);
            this.Controls.Add(this.txt_PretraziKorisnika);
            this.Controls.Add(this.lbl_PretraziKorisnika);
            this.Controls.Add(this.btn_KreirajKorisnika);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "FrmKorisnik";
            this.Text = "FrmKorisnik";
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Korisnici)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_PretraziKorisnika;
        private System.Windows.Forms.TextBox txt_PretraziKorisnika;
        private System.Windows.Forms.Label lbl_PretraziKorisnika;
        private System.Windows.Forms.Button btn_KreirajKorisnika;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dgv_Korisnici;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem knjigeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem knjigeToolStripMenuItem1;
        private System.Windows.Forms.Button btn_PromeniKorisnika;
    }
}