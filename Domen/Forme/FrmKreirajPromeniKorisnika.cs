﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forme
{
    public partial class FrmKreirajPromeniKorisnika : Form
    {
        private Korisnik Korisnik { get; set; }
        public FrmKreirajPromeniKorisnika()
        {
            InitializeComponent();
            
        }

        public FrmKreirajPromeniKorisnika(Korisnik korisnik)
        {
            InitializeComponent();
            Korisnik = korisnik;
            txt_DatumRodjenja.Text = korisnik.DatumRodjenja.ToString("dd.MM.yyyy.");
            txt_ImePrezime.Text = korisnik.ImePrezime;
            cmb_Pol.Text = korisnik.Pol;
            cmb_StrucnaSprema.Text = korisnik.StrucnaSprema;
        }

        private void btn_SacuvajKorisnika_Click(object sender, EventArgs e)
        {
            if(DateTime.TryParseExact(txt_DatumRodjenja.Text,"dd.MM.yyyy.",null,System.Globalization.DateTimeStyles.None,out DateTime datumRodjenja))
            {
                if(cmb_Pol.Text.Length != 0 && cmb_StrucnaSprema.Text.Length != 0 && txt_ImePrezime.Text.Length != 0)
                {
                    if(Korisnik == null)
                    {
                        KontrolerKorisnickogInterfejsa.Instance.SacuvajKorisnika(new Korisnik()
                        {
                            ImePrezime = txt_ImePrezime.Text,
                            DatumRodjenja = datumRodjenja,
                            Pol = cmb_Pol.Text,
                            StrucnaSprema = cmb_StrucnaSprema.Text,
                            Aktivan = chb_Aktivan.Checked
                        });
                        this.Hide();
                        MessageBox.Show("Sistem je uspesno kreirao korisnika.");
                    }
                    else
                    {
                        KontrolerKorisnickogInterfejsa.Instance.IzmeniKorisnika(new Korisnik()
                        {
                            KorisnikID = Korisnik.KorisnikID,
                            ImePrezime = txt_ImePrezime.Text,
                            DatumRodjenja = datumRodjenja,
                            Pol = cmb_Pol.Text,
                            StrucnaSprema = cmb_StrucnaSprema.Text,
                            Aktivan = chb_Aktivan.Checked
                        });
                        this.Hide();
                        MessageBox.Show("Sistem je zapamtio korisnika.");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Nisu sva polja popunjena.");
                    MessageBox.Show("Sistem ne moze da zapamti korisnika");
                }
            }
            else
            {
                MessageBox.Show("Datum rodjenja nije u ispravnom formatu.");
                MessageBox.Show("Sistem ne moze da zapamti korisnika");
            }
        }
    }
}
