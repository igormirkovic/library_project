﻿using Domen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Forme
{
    public partial class FrmKreirajKnjigu : Form
    {
        public FrmKreirajKnjigu()
        {
            InitializeComponent();
            chb_Aktivna.Checked = true;
            cmb_Autor.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiSveAutore();
            cmb_Zanr.DataSource = KontrolerKorisnickogInterfejsa.Instance.VratiSveZanrove();
        }

        private void btn_SacuvajKnjigu_Click(object sender, EventArgs e)
        {
            if(txt_NazivKnjige.Text.Length != 0)
            {
                bool result = KontrolerKorisnickogInterfejsa.Instance.SacuvajKnjigu(new Knjiga()
                {
                    Autor = (Autor)cmb_Autor.SelectedItem,
                    Zanr = (Zanr)cmb_Zanr.SelectedItem,
                    Naziv = txt_NazivKnjige.Text,
                    Aktivna = chb_Aktivna.Checked
                });

                if (result)
                {
                    this.Hide();
                    MessageBox.Show("Sistem je uspesno kreirao knjigu.");
                }
                else
                {
                    MessageBox.Show("Sistem ne moze da kreira knjigu.");
                }
            }
            else
            {
                MessageBox.Show("Naziv knjige nije upisan.");
                MessageBox.Show("Sistem ne moze da kreira knjigu.");
            }

        }
    }
}
