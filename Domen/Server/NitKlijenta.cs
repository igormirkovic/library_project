﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Domen;
using Kontroler;

namespace Server
{
    public class NitKlijenta
    {
        private NetworkStream tok;
        private BinaryFormatter formatter;
        private Kontroler.Kontroler kontroler = Kontroler.Kontroler.Instance;

        public NitKlijenta(NetworkStream tok)
        {
            this.tok = tok;
            formatter = new BinaryFormatter();
            ThreadStart nitObradi = Obradi;
            new Thread(nitObradi).Start();
        }

        public void Obradi()
        {
            try
            {
                int operacija = 0;
                while(operacija != (int)Operacija.Kraj)
                {
                    TransfernaKlasa transfernaKlasa = formatter.Deserialize(tok) as TransfernaKlasa;
                    switch (transfernaKlasa.Operacija)
                    {
                        case Operacija.Kraj:
                            
                            operacija = 1;
                            
                            break;
                        case Operacija.UlogujSe:

                            Bibliotekar bibliotekar = transfernaKlasa.TransferniObjekat as Bibliotekar;
                            bibliotekar = kontroler.Prijava(bibliotekar.KorisnickoIme, bibliotekar.Lozinka);
                            transfernaKlasa.TransferniObjekat = bibliotekar;
                            formatter.Serialize(tok, transfernaKlasa);

                            break;
                        case Operacija.VratiKnjige:

                            List<Knjiga> knjige = kontroler.VratiKnjige();
                            transfernaKlasa.TransferniObjekat = knjige;
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.VratiAutore:

                            List<Autor> autori = kontroler.VratiAutore();
                            transfernaKlasa.TransferniObjekat = autori;
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.VratiZanrove:

                            List<Zanr> zanrovi = kontroler.VratiZanrove();
                            transfernaKlasa.TransferniObjekat = zanrovi;
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.VratiKorisnike:
                            
                            List<Korisnik> korisnici = kontroler.VratiKorisnike();
                            transfernaKlasa.TransferniObjekat = korisnici;
                            formatter.Serialize(tok, transfernaKlasa);

                            break;
                        case Operacija.SacuvajKnjigu:
                            
                            transfernaKlasa.Result = kontroler.SacuvajKnjigu(transfernaKlasa.TransferniObjekat as Knjiga);
                            formatter.Serialize(tok, transfernaKlasa);
                           
                            break;

                        case Operacija.PretraziKnjige:
                            Knjiga knjiga = new Knjiga()
                            {
                                Kriterijum = (string)transfernaKlasa.TransferniObjekat
                            };
                            transfernaKlasa.Result = kontroler.PretraziKnjige(knjiga);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.DeaktivirajKnjigu:

                            transfernaKlasa.Result = kontroler.DeaktivirajKnjigu(transfernaKlasa.TransferniObjekat as Knjiga);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.SacuvajKorisnika:

                            transfernaKlasa.Result = kontroler.SacuvajKorisnika(transfernaKlasa.TransferniObjekat as Korisnik);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.IzmeniKorisnika:

                            transfernaKlasa.Result = kontroler.IzmeniKorisnika(transfernaKlasa.TransferniObjekat as Korisnik);
                            formatter.Serialize(tok, transfernaKlasa);
                            
                            break;

                        case Operacija.PretraziKorisnike:

                            Korisnik korisnik = new Korisnik()
                            {
                                Kriterijum = (string)transfernaKlasa.TransferniObjekat
                            };
                            transfernaKlasa.Result = kontroler.PretraziKorisnike(korisnik);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.VratiPrimerke:

                            transfernaKlasa.Result = kontroler.VratiPrimerke(transfernaKlasa.TransferniObjekat as Knjiga);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.SacuvajPozajmicu:

                            transfernaKlasa.Result = kontroler.SacuvajPozajmicu(transfernaKlasa.TransferniObjekat as Pozajmica) && kontroler.IzmeniPrimerakKnjige(transfernaKlasa.TransferniObjekat as Pozajmica);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.VratiPozajmice:

                            transfernaKlasa.Result = kontroler.VratiPozajmice();
                            formatter.Serialize(tok, transfernaKlasa);

                            break;

                        case Operacija.DeaktivirajPozajmicu:

                            transfernaKlasa.Result = kontroler.DeaktivirajPozajmicu(transfernaKlasa.TransferniObjekat as Pozajmica) && kontroler.IzmeniPrimerakKnjige(transfernaKlasa.TransferniObjekat as Pozajmica);
                            formatter.Serialize(tok, transfernaKlasa);

                            break;
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
