﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server
{
    public partial class FrmServer : Form
    {
        Server s;
        public FrmServer()
        {
            InitializeComponent();
            btn_ZaustaviServer.Enabled = false;
        }

        private void btn_PokreniServer_Click(object sender, EventArgs e)
        {
            s = new Server();
            if (s.PokreniServer())
            {
                txt_StatusServera.Text = "Server je pokrenut";
                btn_PokreniServer.Enabled = false;
                btn_ZaustaviServer.Enabled = true;
            }
        }

        private void btn_ZaustaviServer_Click(object sender, EventArgs e)
        {
            if (s.ZaustaviServer())
            {
                txt_StatusServera.Text = "Server zaustavljen";
                btn_PokreniServer.Enabled = true;
                btn_ZaustaviServer.Enabled = false;
            }
        }
    }
}
