﻿namespace Server
{
    partial class FrmServer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ZaustaviServer = new System.Windows.Forms.Button();
            this.btn_PokreniServer = new System.Windows.Forms.Button();
            this.txt_StatusServera = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btn_ZaustaviServer
            // 
            this.btn_ZaustaviServer.Location = new System.Drawing.Point(300, 116);
            this.btn_ZaustaviServer.Name = "btn_ZaustaviServer";
            this.btn_ZaustaviServer.Size = new System.Drawing.Size(143, 90);
            this.btn_ZaustaviServer.TabIndex = 1;
            this.btn_ZaustaviServer.Text = "Zaustavi server";
            this.btn_ZaustaviServer.UseVisualStyleBackColor = true;
            this.btn_ZaustaviServer.Click += new System.EventHandler(this.btn_ZaustaviServer_Click);
            // 
            // btn_PokreniServer
            // 
            this.btn_PokreniServer.Location = new System.Drawing.Point(54, 116);
            this.btn_PokreniServer.Name = "btn_PokreniServer";
            this.btn_PokreniServer.Size = new System.Drawing.Size(143, 90);
            this.btn_PokreniServer.TabIndex = 2;
            this.btn_PokreniServer.Text = "Pokreni server";
            this.btn_PokreniServer.UseVisualStyleBackColor = true;
            this.btn_PokreniServer.Click += new System.EventHandler(this.btn_PokreniServer_Click);
            // 
            // txt_StatusServera
            // 
            this.txt_StatusServera.Location = new System.Drawing.Point(54, 38);
            this.txt_StatusServera.Name = "txt_StatusServera";
            this.txt_StatusServera.ReadOnly = true;
            this.txt_StatusServera.Size = new System.Drawing.Size(389, 20);
            this.txt_StatusServera.TabIndex = 3;
            this.txt_StatusServera.Text = "Server zaustavljen";
            this.txt_StatusServera.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FrmServer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(502, 258);
            this.Controls.Add(this.txt_StatusServera);
            this.Controls.Add(this.btn_PokreniServer);
            this.Controls.Add(this.btn_ZaustaviServer);
            this.Name = "FrmServer";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ZaustaviServer;
        private System.Windows.Forms.Button btn_PokreniServer;
        private System.Windows.Forms.TextBox txt_StatusServera;
    }
}

