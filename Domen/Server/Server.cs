﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    public class Server
    {
        private Socket osluckujuciSocket;

        public bool PokreniServer()
        {
            try
            {
                osluckujuciSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                osluckujuciSocket.Bind(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 20000));
                ThreadStart nitOsluskivanja = Osluskuj;
                new Thread(nitOsluskivanja).Start();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

       public void Osluskuj()
        {
            try
            {
                while (true)
                {
                    osluckujuciSocket.Listen(8);
                    Socket klijentskiSocket = osluckujuciSocket.Accept();
                    NetworkStream tok = new NetworkStream(klijentskiSocket);
                    new NitKlijenta(tok);
                }
            }
            catch (Exception)
            {

            }
        }

        public bool ZaustaviServer()
        {
            try
            {
                osluckujuciSocket.Close();
                
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }
    }
}
