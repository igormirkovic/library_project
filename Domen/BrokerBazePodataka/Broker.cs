﻿using Domen;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrokerBazePodataka
{
    public class Broker
    {
        public static Broker _instance;
        public static Broker Instance
        {
            get
            {
                if(_instance == null)
                {
                    _instance = new Broker();
                }
                return _instance;
            }
        }

        

        private SqlConnection connection;
        private SqlTransaction transaction;

        public Broker()
        {
            connection = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=BibliotekaDB;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
        }

        public void OtvoriKonekciju()
        {
            connection.Open();
        }

        public void ZatvoriKonekciju()
        {
            connection.Close();
        }

        public void PokreniTransakciju()
        {
            transaction = connection.BeginTransaction();
        }

        public void Commit()
        {
            transaction.Commit();
        }

        public void Rollback()
        {
            transaction.Rollback();
        }

        public List<IDomenskiObjekat> UlogujSe(IDomenskiObjekat objekat)
        {
            Bibliotekar bibliotekar = (Bibliotekar)objekat;
            SqlCommand command = new SqlCommand("", connection);
            command.CommandText = $"{objekat.Get} {objekat.Table} {objekat.SearchWhere(bibliotekar.KorisnickoIme, bibliotekar.Lozinka)}";
            SqlDataReader reader = command.ExecuteReader();
            return objekat.GetReaderResult(reader);
        }

        public List<IDomenskiObjekat> VratiSve(IDomenskiObjekat objekat)
        {
            
                SqlCommand command = new SqlCommand("", connection, transaction);
                command.CommandText = $"{objekat.Get} {objekat.FullTable} {objekat.Join} {objekat.GroupBy}";
                SqlDataReader reader = command.ExecuteReader();
                return objekat.GetReaderResult(reader);
        }

        public List<IDomenskiObjekat> VratiSvePoKriterijumu(IDomenskiObjekat objekat, Knjiga knjiga)
        {
            
            SqlCommand command = new SqlCommand("", connection);
            command.CommandText = $"{objekat.Get} {objekat.FullTable} {objekat.SearchWhere(knjiga.KnjigaID.ToString())}";
            SqlDataReader reader = command.ExecuteReader();
            return objekat.GetReaderResult(reader);
        }

        public bool Sacuvaj(IDomenskiObjekat objekat)
        {
            SqlCommand command = new SqlCommand("", connection, transaction);
            command.CommandText = $"INSERT INTO {objekat.Table} VALUES({objekat.InsertValues})";
            return command.ExecuteNonQuery() == 1;
        }

        public List<IDomenskiObjekat> Pretrazi(IDomenskiObjekat objekat)
        {
            SqlCommand command = new SqlCommand("", connection, transaction);
            command.CommandText = $"{objekat.Get} {objekat.FullTable} {objekat.Join} {objekat.SearchWhere(objekat.Kriterijum)} {objekat.GroupBy}";
            SqlDataReader reader = command.ExecuteReader();
            return objekat.GetReaderResult(reader);
        }

        public bool Izmeni(IDomenskiObjekat objekat)
        {
            SqlCommand command = new SqlCommand("", connection, transaction);
            command.CommandText = $"UPDATE {objekat.Table} SET {objekat.UpdateValues} {objekat.SearchId}";
            return command.ExecuteNonQuery() == 1;
        }
    }
}

